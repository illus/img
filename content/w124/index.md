---
title: ''
date: '2021-06-26T16:02:42+01:00'
draft: false
url: 'meca/w124'

---

## Vends Mercedes w124 E250 Break pour pièces

Je vous ai fait une belle annonce aussi complète que possible,
MERCI DE BIEN LA LIRE EN ENTIER avant de me contacter

Mercedes w124 E250 Break - Diesel
Année 1996 - 380.000 km - diesel - BVM  
Prix: 500€

### Factures récentes:

- 312.000km : joint de cullasse refait en garage - facture dispo
- 342.000km : embrayage + récepteur + butée + bougies de préchauffage + plaquettes arrières neuf
- 358.000km : faisceau de préchauffage refait entièrement
- 380.000km : mise à l'arrêt suite à un accident (estimation de la valeur du véhicule avant accident par l'assurance: 3000€ - le CT était quasi-vierge)

Je me suis fait rentrer dedans sur le flan gauche. Le chassis a pris un coup et la carosserie côté gauche est à oublier.

Elle m'a servie de véhicule pour pièces pendant un moment:

### Pièces démontées ou défectueuses :

- carrosserie flanc gauche, capot, aile AvD
- pare-chocs, échappement
- réservoir déposé mais fourni
- optiques

### Pièces intéressantes :

- moteur en excellent état, culasse très récente (voir facture). Ce moteur atteindra le million de Km
- Boite manuelle en bon état. Embrayage, récepteur et butée récente
- faisceaux de préchauffage entièrement refait (c'est la maladie sur les phases 3, pièce très demandée)
- carte grise à jour, véhicule non gagé
- intérieur propre à part le siège conducteur élimé à gauche
- moteurs vitres électriques à l'avant OK, comodo, calculateur, duovanne, relai préchauffage et autres OK
- spécial Break: amortisseurs BE, antenne motorisée ok, moteur d'essuie-glaçe arrière, haillon en bon état
- possibilité de faire tourner le moteur pour vérification
- accessible et roulante (possibilité de la hisser sur un plateau)

Bref vous l'aurez compris, y'a moyen de se faire un bon billet en revendant en pièces détachées, ou de se faire un bon stock pour vos vieux jours.


### Photos:

{{< gallery
    match="images/*"
    showExif="true"
    sortOrder="name"
    loadJQuery="true"
    embedPreview="true"
    previewType="blur"
    resizeOptions="600x600 q90 Lanczos"
    thumbnailHoverEffect="enlarge"
>}}

### Contact:

Yann: 07.49.31.41.59
